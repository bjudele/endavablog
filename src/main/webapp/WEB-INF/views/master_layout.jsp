<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<html>
<head>
<title><tiles:getAsString name="title" /></title>
</head>
<body>
	<tiles:insertAttribute name="jsHeader" />
	<tiles:insertAttribute name="top" />
	<div class="widewrapper main">
		<div class="container">
			<div class="row">
				<tiles:insertAttribute name="body" />
				<tiles:insertAttribute name="rightSide" />
			</div>
		</div>
	</div>
	
	<tiles:insertAttribute name="footer" />
</body>
</html>

