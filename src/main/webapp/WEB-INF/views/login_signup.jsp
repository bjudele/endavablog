<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ page import="java.util.List"%>
<%@ page import="com.endava.blog.model.User"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form"
	uri="http://www.springframework.org/tags/ajaxform"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

</head>
<body>
	<style>
.backendMessage {
	margin-top: 20px;
	margin-bottom: 0px;
	text-align: center;
}

.panel-login>.backendMessage+.panel-heading {
	margin-top: 10px;
}
</style>
	<div class="col-md-6 col-md-offset-3">

		<div class="panel panel-login">
			<c:if test="${successMessage}">
				<div class="backendMessage alert alert-success alert-dismissable">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<strong>Success!</strong> ${backendMessage}
				</div>
			</c:if>
			<c:if test="${errorMessage}">
				<div class="backendMessage alert alert-danger alert-dismissable">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<strong>Error!</strong> ${backendMessage}
				</div>
			</c:if>
			<div class="panel-heading">
				<div class="row">
					<div class="col-xs-6">
						<a href="#" class="${user.loginFormVisible?'active':'' }"
							id="login-form-link">Login</a>
					</div>
					<div class="col-xs-6">
						<a href="#" class="${user.loginFormVisible?'':'active' }"
							id="register-form-link">Register</a>
					</div>
				</div>
				<hr>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-lg-12">

						<form:form id="login-form" action="j_spring_security_check"
							method="POST" modelAttribute="user"
							cssStyle="display: ${user.loginFormVisible?'block':'none' };">
							<div class="form-group">
								<form:input path="username" name="j_username" tabindex="1"
									cssClass="form-control" title="Username" />

							</div>
							<div class="form-group">
								<form:input path="password" name="j_password" type="password"
									tabindex="2" cssClass="form-control" title="Password" />

							</div>
							<div class="form-group text-center checkbox">
								<input type="checkbox" tabindex="3" class="" name="remember"
									id="remember"> <label for="remember"> Remember
									Me</label>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-sm-6 col-sm-offset-3">
										<input type="submit" name="login-submit" id="login-submit"
											tabindex="4" class="form-control btn btn-login"
											value="Log In">
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-lg-12">
										<div class="text-center">
											<a href="#" tabindex="5" class="forgot-password">Forgot
												Password?</a>
										</div>
									</div>
								</div>
							</div>
						</form:form>

						<form:form id="register-form" modelAttribute="user"
							action="registerUser" method="POST"
							cssStyle="display: ${user.loginFormVisible?'none':'block' };">
							<div class="form-group">
								<form:input path="fullName" name="fullName" tabindex="1"
									cssClass="form-control" title="Full Name" />
								<div id='register-form_fullName_errorloc'></div>
							</div>
							<div class="form-group">
								<form:input path="username" name="username" tabindex="2"
									cssClass="form-control" title="Username" />
							</div>
							<div class="form-group">
								<form:input path="email" name="email" type="email" tabindex="3"
									cssClass="form-control" title="Email Address" />
							</div>
							<div class="form-group">
								<form:input path="password" name="password" type="password"
									tabindex="4" cssClass="form-control" title="Password" />
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-sm-6 col-sm-offset-3">
										<input type="submit" name="register-submit"
											id="register-submit" tabindex="4"
											class="form-control btn btn-register" value="Register Now">
									</div>
								</div>
							</div>
						</form:form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/connect/connect.css" />

<script
	src="${pageContext.request.contextPath}/resources/js/connect/jquery.validate.js"></script>
<script
	src="${pageContext.request.contextPath}/resources/js/connect/connect.js"></script>
</html>