<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<aside class="col-md-4 blog-aside">

	<div class="aside-widget">
		<header>
		<h3>Read next...</h3>
		</header>
		<div class="body">
			<ul class="tales-list">
				<li><a
					href="http://demo.hackerthemes.com/tales/html-version/3.3.6-v3/index.html">Email
						Encryption Explained</a></li>
				<li><a href="#">Selling is a Function of Design.</a></li>
				<li><a href="#">It’s Hard To Come Up With Dummy Titles</a></li>
				<li><a href="#">Why the Internet is Full of Cats</a></li>
				<li><a href="#">Last Made-Up Headline, I Swear!</a></li>
			</ul>
		</div>
	</div>

	<div class="aside-widget">
		<header>
		<h3>Authors Favorites</h3>
		</header>
		<div class="body">
			<ul class="tales-list">
				<li><a
					href="http://demo.hackerthemes.com/tales/html-version/3.3.6-v3/index.html">Email
						Encryption Explained</a></li>
				<li><a href="#">Selling is a Function of Design.</a></li>
				<li><a href="#">It’s Hard To Come Up With Dummy Titles</a></li>
				<li><a href="#">Why the Internet is Full of Cats</a></li>
				<li><a href="#">Last Made-Up Headline, I Swear!</a></li>
			</ul>
		</div>
	</div>

	<div class="aside-widget">
		<header>
		<h3>Tags</h3>
		</header>
		<div class="body clearfix">
			<ul class="tags">
				<li><a href="#">OpenPGP</a></li>
				<li><a href="#">Django</a></li>
				<li><a href="#">Bitcoin</a></li>
				<li><a href="#">Security</a></li>
				<li><a href="#">GNU/Linux</a></li>
				<li><a href="#">Git</a></li>
				<li><a href="#">Homebrew</a></li>
				<li><a href="#">Debian</a></li>
			</ul>
		</div>
	</div>
	</aside>
</body>
</html>