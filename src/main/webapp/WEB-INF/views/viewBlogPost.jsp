<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ page import="java.util.List"%>
<%@ page import="com.endava.blog.model.User"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form"
	uri="http://www.springframework.org/tags/ajaxform"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

</head>
<body>

	<div class="col-md-8 blog-main">
		<section class="ticket"> 
			<section class="ticket-inner">
				<section class="author-and-date">
					<div class="user-avatar avatar-2 default-avatar avatar">
						<c:set var="username" value="${postForm.username }" />
						<c:set var="initial" value="${fn:substring(username, 0, 1)}" />
			
						<div class="default-avatar-inner">${initial }</div>
					</div>
					<span class="author-display-name">${postForm.username }</span>
					<p class="creation-date">
						<fmt:formatDate value="${postForm.date}" pattern="MMMM dd, yyyy" />
					</p>
				</section> 
				<header class="ticket-header">
					<h1>${postForm.title }</h1>
				</header>
				<div class="content">
					<p>${postForm.body }</p>
				</div>
				<div class="tags">
					<c:forEach var="tag" items="${postForm.tagsAsList }">
						<a class="tag" href="#">${tag}</a>
					</c:forEach>
				</div>
			</section> 
		</section>
		<section class="user-replies">
			<header class="replies-title">
				<h4>Replies <small>(${fn:length(postForm.comments)})</small></h4>
			</header>
			<c:forEach var="comment" items="${postForm.comments }">
				<section class="ticket-inner">
					<section class="author-and-date">
						<div class="user-avatar avatar-2 default-avatar avatar">
							<c:set var="author" value="${comment.author}" />
							<c:set var="initial" value="${fn:substring(author, 0, 1)}" />
							<div class="default-avatar-inner">${initial }</div>
						</div>
						<span class="author-display-name">${author }</span>
						<p class="creation-date">
							<fmt:formatDate value="${comment.date}" pattern="MMMM dd, yyyy" />
						</p>
					</section> 
					<div class="vote">
			            <div class="btn-vote">
			              <a class="fa fa-thumbs-o-up" aria-hidden="true"></a>
			               
			               
			              
			                <a class="fa fa-thumbs-o-down" aria-hidden="true"></a>
			               
			               
			              
			            </div>
			        </div>
					<div class="content">
						<p>${comment.body }</p>
					</div>
				</section> 
			</c:forEach>














<%--  <c:forEach var="comment" items="${postForm.comments }"> --%>
<!-- 			<div class="comment"> -->

<!-- 				<section class="author-and-date"> -->
<!-- 				<div class="user-avatar avatar-2 default-avatar avatar"> -->
<%-- 					<c:set var="author" value="${comment.author}" /> --%>
<%-- 					<c:set var="initial" value="${fn:substring(author, 0, 1)}" /> --%>

<%-- 					<div class="default-avatar-inner">${initial }</div> --%>
<!-- 				</div> -->

<%-- 				<span class="author-display-name">${author }</span> --%>
<!-- 				<p class="creation-date"> -->
<%-- 					<fmt:formatDate value="${comment.date}" pattern="MMMM dd, yyyy" /> --%>
<!-- 				</p> -->
<!-- 				</section> -->
<!-- 				<div class="content"> -->
<%-- 					<p>${comment.body}</p> --%>
<!-- 				</div> -->
<!-- 			</div> -->
<%-- 		</c:forEach>  --%>
		
		</section>
		<section class="user-comment"> <header class="comment-title">
		<h4>Write your comment</h4>
		</header> <form:form id="post-form" modelAttribute="commentForm"
			action="addCommentToBlogPost" method="POST">

			<%-- 				<form:input type="hidden" path="permalink" name="${postForm.permalink}" /> --%>

			<div class="form-group">
				<form:textarea path="body" tabindex="1" cssClass="form-control"
					title="What do you think about this?" />
			</div>


			<div class="form-group">
				<div class="row">
					<div class="col-sm-6 col-sm-offset-3">
						<input type="submit" name="register-submit" id="register-submit"
							tabindex="4" class="form-control btn btn-register"
							value="Post now">
					</div>
				</div>
			</div>
		</form:form> </section>






	</div>

</body>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/connect/connect.css" />
<style>
.backendMessage {
	margin-top: 20px;
	margin-bottom: 0px;
	text-align: center;
}

.panel-login>.backendMessage+.panel-heading {
	margin-top: 10px;
}

.ticket .user-avatar .default-avatar-inner,
.user-replies .user-avatar .default-avatar-inner{
	border-radius: 50%;
	color: #fff;
	font-size: 16.67px;
	font-weight: 500;
	height: 42px;
	line-height: 42px;
	margin: 4px;
	text-align: center;
	text-transform: uppercase;
	width: 42px;
	background-color: #459fed;
}

.ticket .user-avatar.default-avatar.avatar-2,
.user-replies .user-avatar.default-avatar.avatar-2 {
	background-color: #fff;
	border: 1px solid #eff1f2;
	border-radius: 50%;
	display: inline-block;
	float: left;
	margin-right: 10px;
}

.ticket p.creation-date,
.user-replies p.creation-date {
	color: #66737c;
	display: block;
	font-size: 14px;
	margin: -4px 0 0;
}

.ticket .ticket-header h1,
.user-replies .ticket-header h1 {
	color: #162d3d;
	font-size: 25px;
	font-weight: 400;
	margin-bottom: 17px;
	margin-top: 0;
	word-break: break-all;
}

body {
	font-family: helvetica-neue, helvetica-neue-latin,
		helvetica-neue-cyrillic, Helvetica, Arial, sans-serif !important;
}

.ticket .ticket-inner::before,
.user-replies .ticket-inner::before {
	background-color: #b9dbf8;
}

.ticket .ticket-inner::after,
.ticket .ticket-inner::before,
.user-replies .ticket-inner::after,
.user-replies .ticket-inner::before {
	border-radius: 3px 0 0;
	content: "";
	height: 20px;
	left: 15px;
	position: absolute;
	top: -10px;
	transform: rotate(45deg);
	width: 20px;
}

.ticket .ticket-inner::after,
.user-replies .ticket-inner::after {
	background-color: #e8f3fd;
	top: -9px;
	z-index: 1;
}

.ticket .ticket-inner::after,
.ticket .ticket-inner::before ,
.user-replies .ticket-inner::after,
.user-replies .ticket-inner::before {
	border-radius: 3px 0 0;
	content: "";
	height: 20px;
	left: 15px;
	position: absolute;
	top: -10px;
	transform: rotate(45deg);
	width: 20px;
}

.ticket .ticket-inner,
.user-replies .ticket-inner {
	background-color: #e8f3fd;
	border: 1px solid #b9dbf8;
	border-radius: 3px;
	display: block;
	margin: 115px auto 50px;
	max-width: 700px;
	padding: 15px 0 10px;
	position: relative;
}

.ticket .ticket-inner::before,
.user-replies .ticket-inner::before {
	background-color: #b9dbf8;
}

.ticket .ticket-inner::after,
.user-replies .ticket-inner::after {
	background-color: #e8f3fd;
	top: -9px;
	z-index: 1;
}

.ticket .author-and-date,
.user-replies .author-and-date {
	position: absolute;
	top: -85px;
	width: 100%;
}

.ticket .ticket-header,
.user-replies .ticket-header {
	padding: 0 23px;
	
}

.ticket .content,
.user-replies .content {
	padding: 0 23px;
	display: inline-block;
	width: calc(100% - 36px);
}

.tags a.tag {
	border: 1px solid #E84423;
	color: #E84423;
	border-radius: 3px;
	display: inline-block;
	margin: 0 0 0.313rem;
	padding: 0.313rem 0.625rem;
	z-index: 50;
	text-decoration: none;
	transition: all 0.25s ease 0s;
	background: transparent none repeat scroll 0 0;
}

div.tags {
	padding: 0 23px;
}

.user-replies .replies-title,.user-comment .comment-title {
	display: block;
	margin: -40px auto 0;
	max-width: 700px;
}

.user-replies .replies-title h4,.user-comment .comment-title h4 {
	background-color: #fff;
	color: #66737c;
	display: inline-block;
	font-size: 21px;
	font-weight: 300;
	margin-left: -25px;
	padding: 0 25px;
	position: relative;
}

.user-replies,.user-comment {
	border-top: 1px solid #eff1f2;
	float: left;
	min-width: 320px;
	padding-bottom: 60px;
	width: 100%;
}
.vote{
	display: inline-block;
	float: right;
    padding-right: 15px;
}
.btn-vote a {
    color: #459FED;
    cursor: pointer;
    display: block;
}
.btn-vote a:hover {
    color: #e84423;
 }
.content > p {
    margin-bottom: 20px;
}
</style>
<script
	src="${pageContext.request.contextPath}/resources/js/connect/jquery.validate.js"></script>
<script
	src="${pageContext.request.contextPath}/resources/js/connect/connect.js"></script>
</html>