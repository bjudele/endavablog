<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:set var="authenticated"
	value="${pageContext.request.userPrincipal.authenticated}" />

<header>
	<div class="widewrapper masthead">
		<div class="container">
			<a href="http://www.endava.com/en/" target="_blank" id="logo">
				Endava Blog <!--Use the following line instead if you want to use an image logo-->
				<!--<img src="img/tales-logo.png" alt="Tales Blog">-->
			</a>

			<div id="mobile-nav-toggle" class="pull-right">
				<a href="#" data-toggle="collapse"
					data-target=".tales-nav .navbar-collapse"> <i
					class="fa fa-bars"></i>
				</a>
			</div>

			<nav class="pull-right tales-nav">
				<div class="collapse navbar-collapse">
					<ul class="nav nav-pills navbar-nav">
					<c:choose>
						<c:when test="${not authenticated}">
							<li class="active"><a
								href="${pageContext.request.contextPath}/login">
									Authenticate </a></li>
						</c:when>
						<c:otherwise>
							<li class="dropdown active"><a class="dropdown-toggle"
							data-toggle="dropdown" href="#">
								${pageContext.request.userPrincipal.name} <b class="caret"></b>
						</a>
							<ul class="dropdown-menu">
								<li><a
									href="${pageContext.request.contextPath}/addBlogPost">New
										post</a></li>
								<li><a href="${pageContext.request.contextPath}/aboutMe">My
										details</a></li>
								<li><a href="${pageContext.request.contextPath}/logout">Logout</a></li>

							</ul></li>
						</c:otherwise>
					</c:choose>							
						<li><a
							href="http://demo.hackerthemes.com/tales/html-version/3.3.6-v3/about.html">About</a>
						</li>
						<li><a
							href="http://demo.hackerthemes.com/tales/html-version/3.3.6-v3/credits.html">Credits</a>
						</li>
					</ul>
				</div>
			</nav>

		</div>
	</div>

	<div class="widewrapper subheader">
		<div class="container">
			<div class="tales-breadcrumb">
				<a href="#">Blog</a>
			</div>

			<div class="tales-searchbox">
				<form action="#" method="get" accept-charset="utf-8">
					<button class="searchbutton" type="submit">
						<i class="fa fa-search"></i>
					</button>
					<input class="searchfield" id="searchbox" placeholder="Search"
						type="text">
				</form>
			</div>
		</div>
	</div>
</header>