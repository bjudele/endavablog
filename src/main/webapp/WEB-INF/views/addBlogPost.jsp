<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ page import="java.util.List"%>
<%@ page import="com.endava.blog.model.User"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form"
	uri="http://www.springframework.org/tags/ajaxform"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

</head>
<body class="addBlogPost">
	<style>
.backendMessage {
	margin-top: 20px;
	margin-bottom: 0px;
	text-align: center;
}

.panel-login>.backendMessage+.panel-heading {
	margin-top: 10px;
}
h1.title{
	margin-top: 0px; 
}
body.addBlogPost .panel-login{
	margin-top: 30px;
}
body.addBlogPost .panel-body{
	margin-top: 38px;
}
body.addBlogPost .panel-body textarea{
	max-width: 718px; 
}

</style>
	<div class="col-md-8 blog-main">
<h1 class="title">An insightful new post</h1>
		<div class="panel panel-login">
			<c:if test="${successMessage}">
				<div class="backendMessage alert alert-success alert-dismissable">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<strong>Success!</strong> ${backendMessage}
				</div>
			</c:if>
			<c:if test="${errorMessage}">
				<div class="backendMessage alert alert-danger alert-dismissable">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<strong>Error!</strong> ${backendMessage}
				</div>
			</c:if>
<!-- 			<div class="panel-heading"> -->
<!-- 				<div class="row"> -->
<!-- 					<div class="col-xs-6"> -->
<!-- 						<a href="#" class="active" -->
<!-- 							id="login-form-link">Add a blog post</a> -->
<!-- 					</div> -->
	
<!-- 				</div> -->
<!-- 				<hr> -->
<!-- 			</div> -->
			<div class="panel-body">
				<div class="row">
					<div class="col-lg-12">


						<form:form id="post-form" modelAttribute="postForm"
							action="addBlogPost" method="POST">
							<div class="form-group">
								<form:input path="title" name="title" tabindex="1"
									cssClass="form-control" title="Title" />
								<div id='register-form_fullName_errorloc'></div>
							</div>
							<div class="form-group">
								<form:textarea path="body" tabindex="2"
									cssClass="form-control" title="Blog entry" />
							</div>
							<div class="form-group">
								<form:input path="tags" name="tags" type="tags" tabindex="3"
									cssClass="form-control" title="Tags (comma separated)" />
							</div>
							
							<div class="form-group">
								<div class="row">
									<div class="col-sm-6 col-sm-offset-3">
										<input type="submit" name="register-submit"
											id="register-submit" tabindex="4"
											class="form-control btn btn-register" value="Post now">
									</div>
								</div>
							</div>
						</form:form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/connect/connect.css" />

<script
	src="${pageContext.request.contextPath}/resources/js/connect/jquery.validate.js"></script>
<script
	src="${pageContext.request.contextPath}/resources/js/connect/connect.js"></script>
</html>