<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.List"%>
<%@ page import="com.endava.blog.model.User"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form"
	uri="http://www.springframework.org/tags/ajaxform"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<body>

	<div class="col-md-8 blog-main">
		<div class="row">
			<c:forEach var="post" items="${postForms}">
				<div class="col-md-6 col-sm-6">
					<article class=" blog-teaser"> <header>
					<h3>
						<a href="seePost/${post.permalink}">${post.title}</a>
					</h3>
					<span class="meta">by ${post.username} on <fmt:formatDate
							value="${post.date}" pattern="MMMM dd, yyyy" /></span>
					<hr>
					</header>
					<div class="body">${post.body}</div>
					<div class="clearfix">
						<a href="seePost/${post.permalink}" class="btn btn-tales-one">Read
							more</a>
					</div>
					</article>
				</div>
			</c:forEach>
		</div>
		<div class="paging">
			<c:if test="${hasNewer}">
				<a href="newer" class="newer"><i class="fa fa-long-arrow-left"></i>
					Newer</a>
			</c:if>

			<c:if test="${hasOlder}">
				<a href="older" class="older">Older <i
					class="fa fa-long-arrow-right"></i></a>
			</c:if>

		</div>
	</div>

</body>
</html>