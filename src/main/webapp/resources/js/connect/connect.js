$(function() {
	$(document).ready()
	{
		$('#login-form-link').click(function(e) {
			$("#login-form").delay(100).fadeIn(100);
			$("#register-form").fadeOut(100);
			$('#register-form-link').removeClass('active');
			$(this).addClass('active');
			e.preventDefault();
		});
		$('#register-form-link').click(function(e) {
			$("#register-form").delay(100).fadeIn(100);
			$("#login-form").fadeOut(100);
			$('#login-form-link').removeClass('active');
			$(this).addClass('active');
			e.preventDefault();
		});
		$("input").each(function() {
			$(this).attr("placeholder", $(this).attr("title"));
		});

		$('#register-form').validate({ // initialize the plugin
			rules : {
				fullName : {
					required : true,
					minlength : 5
				},
				username : {
					required : true,
					minlength : 5
				},
				email : {
					required : true,
					email : true
				},
				password : {
					required : true,
					minlength : 5
				}

			},
			messages : {
				fullName : "Enter your full name",
				email : "Enter your email address",
				username : {
					required : "Enter a username",
					minlength : "Enter at least {0} characters",
					remote : "{0} is already in use"
				},
				password : {
					required : "You need a password for your account",
					minlength : "Enter at least {0} characters",
					remote : "{0} is already in use"
				}
			}
		});

		$('#login-form').validate({ // initialize the plugin
			rules : {
			
				username : {
					required : true,
					minlength : 5
				},
		
				password : {
					required : true,
					minlength : 5
				}

			},
			messages : {
				
				
				username : {
					required : "Enter a username",
					minlength : "Your username has at least {0} characters",
					remote : "{0} is already in use"
				},
				password : {
					required : "Please enter your password",
					minlength : "Your password has at least {0} characters",
					remote : "{0} is already in use"
				}
			}
		});
	}
});
