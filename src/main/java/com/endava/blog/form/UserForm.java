package com.endava.blog.form;

public class UserForm {
	private String fullName;
	private String email;
	private String username;
	private String password;
	private String sessionId;
	private Boolean loginFormVisible = true;

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public Boolean getLoginFormVisible() {
		return loginFormVisible;
	}

	public void setLoginFormVisible(Boolean loginFormVisible) {
		this.loginFormVisible = loginFormVisible;
	}

}
