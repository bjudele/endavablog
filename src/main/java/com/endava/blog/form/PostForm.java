package com.endava.blog.form;

import java.util.Date;
import java.util.List;

import com.endava.blog.model.Comment;

public class PostForm {
	private String title;
	private String body;
	private List<String> tagsAsList;
	private String tags;
	private String username;
	private String permalink;
	private Date date;
	private List<Comment> comments;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPermalink() {
		return permalink;
	}

	public void setPermalink(String permalink) {
		this.permalink = permalink;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	public List<String> getTagsAsList() {
		return tagsAsList;
	}

	public void setTagsAsList(List<String> tagsAsList) {
		this.tagsAsList = tagsAsList;
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

}
