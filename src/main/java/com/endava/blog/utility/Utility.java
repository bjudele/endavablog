package com.endava.blog.utility;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import com.endava.blog.form.CommentForm;
import com.endava.blog.form.PostForm;
import com.endava.blog.form.UserForm;
import com.endava.blog.model.Comment;
import com.endava.blog.model.Post;
import com.endava.blog.model.User;

public class Utility {
	public static User userFormToUser(UserForm userForm) {
		User user = new User();
		user.setEmail(userForm.getEmail());
		user.setFullName(userForm.getFullName());
		user.setPassword(userForm.getPassword());
		user.setUsername(userForm.getUsername());
		return user;
	}

	public static Post postFormToPost(PostForm postForm, String username) {
		Post post = new Post();
		post.setUsername(username);
		post.setTitle(postForm.getTitle());
		post.setBody(postForm.getBody());
		post.setDate(new Date());
		List<String> tags = Arrays.asList(postForm.getTags().split("[, ]+"));
		post.setTags(tags);
		List<Comment> comments = new ArrayList<Comment>();
		post.setComments(comments);
		post.setPermalink(createPermalink(postForm.getTitle()));
		return post;
	}

	public static Comment commentFormToComment(CommentForm commentForm,
			String username) {
		Comment comment = new Comment();
		comment.setAuthor(username);
		comment.setDate(new Date());
		comment.setBody(commentForm.getBody());
		return comment;
	}

	private static String createPermalink(String title) {
		String permalink = title.replaceAll("\\s", "_"); // whitespace becomes _
		permalink = permalink.replaceAll("\\W", ""); // get rid of non
														// alphanumeric
		permalink = permalink.toLowerCase();

		String permLinkExtra = String.valueOf(GregorianCalendar.getInstance()
				.getTimeInMillis());
		return permalink + permLinkExtra;
	}

	public static PostForm postToPostForm(Post post, Boolean upTo200Chars) {

		PostForm postForm = new PostForm();

		if (upTo200Chars && post.getBody().length() > 200) {
			postForm.setBody(post.getBody().substring(0, 200) + " (...)");

		} else {
			postForm.setBody(post.getBody());
		}
		
		postForm.setTagsAsList(post.getTags());
		postForm.setComments(post.getComments());
		postForm.setDate(post.getDate());
		postForm.setTitle(post.getTitle());
		postForm.setUsername(post.getUsername());
		postForm.setPermalink(post.getPermalink());
		return postForm;
	}

	public static List<PostForm> postsToPostForms(List<Post> posts) {
		List<PostForm> postForms = new ArrayList<PostForm>();
		for (Post post : posts) {

			postForms.add(postToPostForm(post, true));
		}
		return postForms;
	}

}
