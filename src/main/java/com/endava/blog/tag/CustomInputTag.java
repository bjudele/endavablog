package com.endava.blog.tag;

import javax.servlet.jsp.JspException;

import org.springframework.web.servlet.tags.form.InputTag;
import org.springframework.web.servlet.tags.form.TagWriter;

public class CustomInputTag extends InputTag {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5858772908435194618L;

	public static final String TYPE_ATTRIBUTE = "type";

	private String type;
	private String name;

	/**
	 * Set the value of the '<code>type</code>' attribute. May be a runtime
	 * expression.
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * Get the value of the '<code>type</code>' attribute.
	 */
	protected String getType() {
		return (this.type == null ? "text" : this.type);
	}
	/**
	 * Set the value of the '<code>name</code>' attribute. May be a runtime
	 * expression.
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * Get the value of the '<code>type</code>' attribute.
	 */
	protected String getName() {
		return (this.name == null ? "" : this.name);
	}
	private boolean hasDynamicTypeAttribute() {
		return getDynamicAttributes() != null
				&& getDynamicAttributes().containsKey("type");
	}

	/**
	 * Writes the '<code>value</code>' attribute to the supplied
	 * {@link TagWriter}. Subclasses may choose to override this implementation
	 * to control exactly when the value is written.
	 */
	protected void writeValue(TagWriter tagWriter) throws JspException {
		String value = getDisplayString(getBoundValue(), getPropertyEditor());
		String type = hasDynamicTypeAttribute() ? (String) getDynamicAttributes()
				.get("type") : getType();
		tagWriter.writeAttribute("value",
				processFieldValue(getName(), value, type));
	}

	/**
	 * Flags {@code type="checkbox"} and {@code type="radio"} as illegal dynamic
	 * attributes.
	 */
	@Override
	protected boolean isValidDynamicAttribute(String localName, Object value) {
		if ("type".equals(localName)) {
			if ("checkbox".equals(value) || "radio".equals(value)) {
				return false;
			}
		}
		return true;
	}

}
