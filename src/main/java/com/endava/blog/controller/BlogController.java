package com.endava.blog.controller;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.endava.blog.form.CommentForm;
import com.endava.blog.form.PostForm;
import com.endava.blog.form.UserForm;
import com.endava.blog.model.User;
import com.endava.blog.services.PostsService;
import com.endava.blog.services.UsersService;

@Controller
@RequestMapping("/")
public class BlogController {

	@Autowired
	private UsersService userService;
	@Autowired
	private PostsService postService;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView index(ModelMap modelMap, HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView("masterLayout");
	
		Date date = new Date();
		List<PostForm> postForms = null;
	
		Boolean fromNewerRedirect = (Boolean) modelMap
				.get("fromNewerRedirect");
		Boolean fromOlderRedirect = (Boolean) modelMap
				.get("fromOlderRedirect");	
		
		if(fromNewerRedirect!= null && fromNewerRedirect){
			date = (Date) request.getSession().getAttribute("upperTimeLimit");
			postForms = postService.findFourPostsAfterThisDate(date);
			modelAndView.addObject("hasNewer", postService.hasPostBeforeThisDate( postForms.get(0).getDate()));
			modelAndView.addObject("hasOlder", true);
		}else if(fromOlderRedirect!= null && fromOlderRedirect){
			date = (Date) request.getSession().getAttribute("lowerTimeLimit");
			postForms = postService.findFourPostsBeforeThisDate(date);
			modelAndView.addObject("hasNewer", true);
			if(postForms.size()<4){
				modelAndView.addObject("hasOlder", false);
			} else{
				modelAndView.addObject("hasOlder", postService.hasPostAfterThisDate( postForms.get(postForms.size()-1).getDate()));
			}
		} else{
			postForms = postService.findFourPostsBeforeThisDate(date);
			
			
			modelAndView.addObject("hasNewer", false);
			modelAndView.addObject("hasOlder", postService.hasPostAfterThisDate( postForms.get(postForms.size()-1).getDate()));
		}
		request.getSession().setAttribute("upperTimeLimit", postForms.get(0).getDate());
		request.getSession().setAttribute("lowerTimeLimit", postForms.get(postForms.size()-1).getDate());
		modelAndView.addObject("postForms", postForms);

		return modelAndView;
	}
	@RequestMapping(value = "/older", method = RequestMethod.GET)
	public String older(ModelMap model, HttpServletRequest request, RedirectAttributes redirectAttributes) {
		ModelAndView modelAndView = new ModelAndView("masterLayout");
		
		redirectAttributes.addFlashAttribute("fromOlderRedirect",
				true);
		redirectAttributes.addFlashAttribute("fromNewerRedirect",
				false);
		if(true)
		return "redirect:/";
		Date date = (Date) request.getSession().getAttribute("lowerTimeLimit");
		
		List<PostForm> postForms = postService.findFourPostsBeforeThisDate(date);
		request.getSession().setAttribute("upperTimeLimit", postForms.get(0).getDate());
		request.getSession().setAttribute("lowerTimeLimit", postForms.get(postForms.size()-1).getDate());
		modelAndView.addObject("postForms", postForms);
		if(postForms.size()<4){
			modelAndView.addObject("hasOlder", false);
		} else{
			modelAndView.addObject("hasOlder", postService.hasPostAfterThisDate( postForms.get(postForms.size()-1).getDate()));
		}
		modelAndView.addObject("hasNewer", true);
		return "masterLayout";
	}
	@RequestMapping(value = "/newer", method = RequestMethod.GET)
	public String newer(ModelMap model, HttpServletRequest request, RedirectAttributes redirectAttributes) {
		redirectAttributes.addFlashAttribute("fromOlderRedirect",
				false);
		redirectAttributes.addFlashAttribute("fromNewerRedirect",
				true);
		if(true)
		return "redirect:/";
		ModelAndView modelAndView = new ModelAndView("masterLayout");
		Date date = (Date) request.getSession().getAttribute("upperTimeLimit");
		
		
		List<PostForm> postForms = postService.findFourPostsAfterThisDate(date);
		request.getSession().setAttribute("upperTimeLimit", postForms.get(0).getDate());
		request.getSession().setAttribute("lowerTimeLimit", postForms.get(postForms.size()-1).getDate());
		modelAndView.addObject("hasNewer", postService.hasPostBeforeThisDate( postForms.get(0).getDate()));
		modelAndView.addObject("hasOlder", true);
		modelAndView.addObject("postForms", postForms);
		
		return "masterLayout";
	}
	@RequestMapping("/login")
	public ModelAndView login(ModelMap modelMap) {
		ModelAndView modelAndView = new ModelAndView();
		UserForm userForm = new UserForm();
		checkRedirectParams(modelMap, userForm);
		modelAndView.addObject("user", userForm);
		return modelAndView;
	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logoutPage(HttpServletRequest request,
			HttpServletResponse response) {
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		if (auth != null) {
			new SecurityContextLogoutHandler().logout(request, response, auth);
		}
		return "redirect:/login?logout";// You can redirect wherever you want,
										// but generally it's a good practice to
										// show login screen again.
	}

	private void checkRedirectParams(ModelMap modelMap, UserForm userForm) {
		modelMap.put("errorMessage", false);
		Boolean unsuccessfulRegistration = (Boolean) modelMap
				.get("unsuccessfulRegistration");
		if (unsuccessfulRegistration != null) {
			if (unsuccessfulRegistration) {
				modelMap.put("backendMessage",
						"Your registration was not successful!");
				modelMap.put("errorMessage", true);
				userForm.setLoginFormVisible(false);
				modelMap.put("unsuccessfulRegistration", null);
			} else {
				modelMap.put("backendMessage", "Your account was registered!");
				modelMap.put("successMessage", true);
				userForm.setLoginFormVisible(false);
				modelMap.put("unsuccessfulRegistration", null);
			}
		}
		Boolean unsuccessfulLogin = (Boolean) modelMap.get("unsuccessfulLogin");
		if (unsuccessfulLogin != null && unsuccessfulLogin) {
			modelMap.put("backendMessage", "Incorrect username or password!");
			modelMap.put("errorMessage", true);
			userForm.setLoginFormVisible(true);
			modelMap.put("unsuccessfulLogin", false);
		}

	}

	@RequestMapping(value = "/registerUser", method = RequestMethod.POST)
	public String registerPost(@ModelAttribute UserForm userForm,
			BindingResult result, ModelMap modelMap,
			RedirectAttributes redirectAttributes) {
		User userDb = userService.insert(userForm);
		if (userDb == null)
			redirectAttributes.addFlashAttribute("unsuccessfulRegistration",
					true);
		else
			redirectAttributes.addFlashAttribute("unsuccessfulRegistration",
					false);

		return "redirect:login";
	}

	@RequestMapping(value = "/successfulRegistration", method = RequestMethod.GET)
	public String successfulRegistration(@ModelAttribute UserForm userForm,
			BindingResult result, RedirectAttributes redirectAttributes,
			HttpServletResponse response) {

		userForm.setLoginFormVisible(false);
		redirectAttributes.addFlashAttribute("unsuccessfulLogin", true);
		return "redirect:login";
	}

	@RequestMapping(value = "/accessdenied", method = RequestMethod.GET)
	public String accessdenied(@ModelAttribute UserForm userForm,
			BindingResult result, RedirectAttributes redirectAttributes,
			HttpServletResponse response) {

		userForm.setLoginFormVisible(false);
		redirectAttributes.addFlashAttribute("unsuccessfulLogin", true);
		return "redirect:login";
	}

	@RequestMapping(value = "/addBlogPost", method = RequestMethod.GET)
	public ModelAndView addBlogPostGet(ModelAndView modelAndView) {
		PostForm postForm = new PostForm();
		modelAndView.addObject("postForm", postForm);
		return modelAndView;

	}

	@RequestMapping(value = "/addBlogPost", method = RequestMethod.POST)
	public String addBlogPostPost(@ModelAttribute PostForm postForm,
			BindingResult result, RedirectAttributes redirectAttributes) {
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		String permalink = postService.addPost(postForm, auth.getName());

		return "redirect:seePost/" + permalink;
	}

	@RequestMapping(value = "/seePost/addCommentToBlogPost", method = RequestMethod.POST)
	public String addCommentToBlogPost(@ModelAttribute CommentForm commentForm,
			BindingResult result, HttpServletRequest request,
			RedirectAttributes redirectAttributes) {
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();

		String permalink = (String) request.getSession().getAttribute(
				"permalink");
		commentForm.setPermalink(permalink);
		Boolean success = postService.addCommentToPost(commentForm,
				auth.getName());

		return "redirect:/seePost/" + commentForm.getPermalink();
	}

	@RequestMapping("/seePost/{permalink}")
	public ModelAndView seePost(
			@PathVariable(value = "permalink") String permalink,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		System.out.println(permalink);
		ModelAndView modelAndView = new ModelAndView("viewBlogPost");
		PostForm postForm = postService.findPostByPermalink(permalink);
		if (postForm == null) {
			return new ModelAndView("redirect:/page_not_found");
		}
		CommentForm commentForm = new CommentForm();
		commentForm.setPermalink(permalink);
		modelAndView.addObject("postForm", postForm);
		modelAndView.addObject("commentForm", commentForm);
		request.getSession().setAttribute("permalink", permalink);
		return modelAndView;
	}

}