package com.endava.blog.services;

import sun.misc.BASE64Encoder;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.endava.blog.form.CommentForm;
import com.endava.blog.form.PostForm;
import com.endava.blog.form.UserForm;
import com.endava.blog.model.Comment;
import com.endava.blog.model.Post;
import com.endava.blog.model.Session;
import com.endava.blog.model.User;
import com.endava.blog.repository.PostRepository;
import com.endava.blog.repository.SessionRepository;
import com.endava.blog.repository.UserRepository;
import com.endava.blog.utility.Utility;

@Service
public class PostsService {

	@Autowired
	private PostRepository postRepository;

	private Random random = new SecureRandom();

	public PostRepository getPostRepository() {
		return postRepository;
	}

	public void setPostRepository(PostRepository postRepository) {
		this.postRepository = postRepository;
	}

	public String addPost(PostForm postForm, String username) {
		Post post = Utility.postFormToPost(postForm, username);
		postRepository.save(post);
		return post.getPermalink();
	}

	public Boolean addCommentToPost(CommentForm commentForm, String username) {
		Post post = postRepository.findByPermalink(commentForm.getPermalink());
		if (post == null)
			return false;
		Comment comment = Utility.commentFormToComment(commentForm, username);
		post.getComments().add(comment);
		postRepository.save(post);
		return true;
	}

	public PostForm findPostByPermalink(String permalink) {
		Post post = postRepository.findByPermalink(permalink);
		return Utility.postToPostForm(post, false);
	}

	public List<PostForm> findFourPostsBeforeThisDate(Date date) {
		Pageable topFour = new PageRequest(0, 4);
		List<Post> posts = postRepository.findTop4ByDateBeforeOrderByDateDesc(
				date, topFour);
		return Utility.postsToPostForms(posts);
	}

	public List<PostForm> findFourPostsAfterThisDate(Date date) {
		Pageable topFour = new PageRequest(0, 4);
		List<Post> posts = postRepository.findTop4ByDateAfterOrderByDateDesc(
				date, topFour);
		return Utility.postsToPostForms(posts);
	}
	public Boolean  hasPostBeforeThisDate(Date date) {
		Pageable top = new PageRequest(0, 1);
		List<Post> posts = postRepository.findTopByDateAfterOrderByDateDesc(
				date, top);
	
		return posts.size() > 0;
	}
	public Boolean hasPostAfterThisDate(Date date) {
		Pageable top = new PageRequest(0, 1);
		List<Post> posts = postRepository.findTopByDateBeforeOrderByDateDesc(
				date, top);
		return posts.size() > 0;
	}
}