package com.endava.blog.services;

import sun.misc.BASE64Encoder;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.endava.blog.form.PostForm;
import com.endava.blog.form.UserForm;
import com.endava.blog.model.Post;
import com.endava.blog.model.Session;
import com.endava.blog.model.User;
import com.endava.blog.repository.PostRepository;
import com.endava.blog.repository.SessionRepository;
import com.endava.blog.repository.UserRepository;
import com.endava.blog.utility.Utility;

@Service
public class UsersService {

	@Autowired
	private UserRepository userRepository;
	@Autowired
	private SessionRepository sessionRepository;

	public User findUserById(String id) {
		return userRepository.findOne(id);
	}

	public Session findSessionById(String id) {
		return sessionRepository.findOne(id);
	}

	public List<User> findAllUsers() {
		return userRepository.findAll();
	}

	public User findByUsername(String username) {
		return userRepository.findByUsername(username);
	}

	private Random random = new SecureRandom();

	public User insert(UserForm userForm) {
		// userForm.setPassword(makePasswordHash(userForm.getPassword(),
		// Integer.toString(random.nextInt())));
		User user = userRepository.findByUsername(userForm.getUsername());
		if (user != null)
			return null;

		User newUser = userFormToUser(userForm);
		newUser.setRole(1);
		newUser.setPassword(userForm.getPassword());

		return userRepository.save(newUser);
	}

	private User userFormToUser(UserForm userForm) {
		User user = new User();
		user.setEmail(userForm.getEmail());
		user.setFullName(userForm.getFullName());
		user.setPassword(userForm.getPassword());
		user.setUsername(userForm.getUsername());
		return user;
	}

	public User update(User user) {
		// image.setUpdateTime(new Date());
		return userRepository.save(user);
	}

	public Boolean validateLogin(UserForm userForm) {

		User dbUser = findByUsername(userForm.getUsername());

		if (dbUser == null) {
			System.out.println("User not in database");
			return false;
		}

		String hashedAndSalted = dbUser.getPassword();

		String salt = hashedAndSalted.split(",")[1];

		if (!hashedAndSalted.equals(makePasswordHash(userForm.getPassword(),
				salt))) {
			System.out.println("Submitted password is not a match");
			return false;
		}

		return true;
	}

	private String makePasswordHash(String password, String salt) {
		try {
			String saltedAndHashed = password + "," + salt;
			MessageDigest digest = MessageDigest.getInstance("MD5");
			digest.update(saltedAndHashed.getBytes());
			BASE64Encoder encoder = new BASE64Encoder();
			byte hashedBytes[] = (new String(digest.digest(), "UTF-8"))
					.getBytes();
			return encoder.encode(hashedBytes) + "," + salt;
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException("MD5 is not available", e);
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException("UTF-8 unavailable?  Not a chance", e);
		}
	}

	public String startSession(String username) {

		// get 32 byte random number. that's a lot of bits.
		SecureRandom generator = new SecureRandom();
		byte randomBytes[] = new byte[32];
		generator.nextBytes(randomBytes);

		BASE64Encoder encoder = new BASE64Encoder();

		String sessionID = encoder.encode(randomBytes);
		Session session = new Session();
		session.set_id(sessionID);
		session.setUsername(username);
		session.setActive(true);
		List<Session> sessions = sessionRepository.findByUsernameAndActive(
				username, true);
		for (Session s : sessions) {
			s.setActive(false);
			sessionRepository.save(s);
		}
		sessionRepository.save(session);
		return session.get_id();
	}

}