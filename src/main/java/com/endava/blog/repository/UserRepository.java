package com.endava.blog.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.endava.blog.model.User;

/*As of Spring 2.5, this annotation also serves as a specialization of @Component, 
 allowing for implementation classes to be autodetected through classpath scanning.

 */
@Repository
public interface UserRepository extends MongoRepository<User, String> {
	 User findByUsername(String username);
	 
}