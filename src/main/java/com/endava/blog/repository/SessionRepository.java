package com.endava.blog.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.endava.blog.model.Session;

@Repository
public interface SessionRepository extends MongoRepository<Session, String> {
//	List<Session> findBySessionId(String _id);
//
//	void delete(Session deleted);
//
//	List<Session> findAll();
//
//	Session save(Session persisted);
	List<Session> findByUsernameAndActive(String username, Boolean active);
	
}