package com.endava.blog.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.endava.blog.model.Post;

@Repository
public interface PostRepository extends MongoRepository<Post, String> {
	Post findByPermalink(String permalink);


//	List<Post> findByDateBefore(Date date);
	List<Post> findTop4ByDateBeforeOrderByDateDesc(Date date, Pageable pageable);
	List<Post> findTop4ByDateAfterOrderByDateDesc(Date date, Pageable pageable);
	List<Post> findTopByDateBeforeOrderByDateDesc(Date date, Pageable pageable);
	List<Post> findTopByDateAfterOrderByDateDesc(Date date, Pageable pageable);
}